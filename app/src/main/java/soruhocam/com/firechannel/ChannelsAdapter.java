package soruhocam.com.firechannel;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import soruhocam.com.firechannellib.model.Channel;

/**
 * Created by mertsimsek on 11/07/16.
 */
public class ChannelsAdapter extends RecyclerView.Adapter<ChannelsAdapter.ChannelItemViewHolder>{

    public interface OnItemClickedListener{
        void onItemClicked(Channel channel);
    }

    List<Channel> channelList;

    private final OnItemClickedListener onItemClickedListener;

    public ChannelsAdapter(OnItemClickedListener onItemClickedListener) {
        channelList = new ArrayList<>();
        this.onItemClickedListener = onItemClickedListener;
    }

    public void addChannel(Channel channel){
        channelList.add(channel);
        notifyDataSetChanged();
    }

    @Override
    public ChannelItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_channel_list, parent, false);
        return new ChannelItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ChannelItemViewHolder holder, int position) {
        holder.bind(channelList.get(position));
    }

    @Override
    public int getItemCount() {
        return channelList.size();
    }

    public class ChannelItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView textViewChannelName;
        Channel channel;

        public ChannelItemViewHolder(View itemView) {
            super(itemView);
            textViewChannelName = (TextView) itemView.findViewById(R.id.textViewChannelName);
            itemView.setOnClickListener(this);
        }

        public void bind(Channel channel){
            this.channel = channel;
            textViewChannelName.setText(channel.name);
        }

        @Override
        public void onClick(View v) {
            onItemClickedListener.onItemClicked(channel);
        }
    }
}
