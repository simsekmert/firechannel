package soruhocam.com.firechannel;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import soruhocam.com.firechannellib.model.Message;

/**
 * Created by mertsimsek on 11/07/16.
 */
public class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.MessageItemViewHolder>{

    List<Message> messageList;

    public MessagesAdapter() {
        messageList = new ArrayList<>();
    }

    public void addMessage(Message message){
        messageList.add(message);
        notifyDataSetChanged();
    }

    @Override
    public MessageItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_messages_list, parent, false);
        return new MessageItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MessageItemViewHolder holder, int position) {
        holder.bind(messageList.get(position));
    }

    @Override
    public int getItemCount() {
        return messageList.size();
    }

    public class MessageItemViewHolder extends RecyclerView.ViewHolder{

        TextView textViewMessageText;
        TextView textViewUserName;
        Message message;

        public MessageItemViewHolder(View itemView) {
            super(itemView);
            textViewMessageText = (TextView) itemView.findViewById(R.id.textViewMessageText);
            textViewUserName = (TextView) itemView.findViewById(R.id.textViewUser);
        }

        public void bind(Message message){
            this.message = message;
            textViewMessageText.setText(message.messageText);
            textViewUserName.setText(message.senderId);
        }

    }
}
