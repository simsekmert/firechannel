package soruhocam.com.firechannel;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.Calendar;
import java.util.List;

import rx.functions.Action1;
import soruhocam.com.firechannellib.firechannel.FireChannel;
import soruhocam.com.firechannellib.model.Channel;
import soruhocam.com.firechannellib.model.Message;

public class MainActivity extends AppCompatActivity implements ChannelsAdapter.OnItemClickedListener{

    RecyclerView recyclerView;
    ChannelsAdapter channelsAdapter;

    EditText edittextChannelName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerChannelList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        channelsAdapter = new ChannelsAdapter(this);
        recyclerView.setAdapter(channelsAdapter);
        setSupportActionBar(toolbar);

        FireChannel.getInstance("/channels/").loadChannels().subscribe(new Action1<Channel>() {
            @Override
            public void call(Channel channel) {
                channelsAdapter.addChannel(channel);
            }
        });

        FireChannel.getInstance("/channels/").loadChannelMessages("-KMPB8zAloee-eyo9_Zy").subscribe(new Action1<Message>() {
            @Override
            public void call(Message message) {
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_add_channel) {
            MaterialDialog materialDialog = new MaterialDialog.Builder(this)
                    .title("Create Channel")
                    .customView(R.layout.dialog_channel_create, true)
                    .positiveText("CREATE")
                    .autoDismiss(false)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            if(edittextChannelName.getText().toString().length() > 0 ){
                                Channel channel = new Channel();
                                channel.createdTime = Calendar.getInstance().getTime().toString();
                                channel.name = edittextChannelName.getText().toString();
                                FireChannel.getInstance("/channels/").createChannel(channel).subscribe();
                                dialog.dismiss();
                            }
                        }
                    })
                    .show();

            View view = materialDialog.getCustomView();
            edittextChannelName = (EditText) view.findViewById(R.id.edittextChannelName);


            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClicked(Channel channel) {
        Intent intent = new Intent(MainActivity.this, MessagesActivity.class);
        intent.putExtra("channelId", channel.id);
        startActivity(intent);
    }
}
