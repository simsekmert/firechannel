package soruhocam.com.firechannel;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import rx.functions.Action1;
import soruhocam.com.firechannellib.firechannel.FireChannel;
import soruhocam.com.firechannellib.model.Message;

/**
 * Created by mertsimsek on 11/07/16.
 */
public class MessagesActivity extends AppCompatActivity{

    RecyclerView recyclerView;
    MessagesAdapter adapter;
    Button buttonSend;
    EditText editTextSend;
    String channelId;
    String userId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);

        channelId = getIntent().getStringExtra("channelId");
        userId = "USER" + (int)(Math.random() * 120);

        buttonSend = (Button) findViewById(R.id.buttonSend);
        editTextSend = (EditText) findViewById(R.id.edittextMessage);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerMessages);
        adapter = new MessagesAdapter();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

        FireChannel.getInstance("/channels/").loadChannelMessages(channelId).subscribe(new Action1<Message>() {
            @Override
            public void call(Message message) {
                adapter.addMessage(message);
                recyclerView.smoothScrollToPosition(adapter.getItemCount() - 1);
            }
        });

        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editTextSend.getText().toString().trim().length()>0){
                    Message message = new Message();
                    message.senderId = userId;
                    message.messageText = editTextSend.getText().toString();
                    FireChannel.getInstance("/channels/").sendMessage(channelId, message).subscribe();
                    editTextSend.setText("");
                }
            }
        });
    }
}
