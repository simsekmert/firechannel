package soruhocam.com.firechannellib.model;

import java.util.HashMap;

/**
 * Created by mertsimsek on 20/06/16.
 */
public class Channel {
    public String id;
    public String name;
    public String createdTime;
    HashMap<String, Message> messages;
}
