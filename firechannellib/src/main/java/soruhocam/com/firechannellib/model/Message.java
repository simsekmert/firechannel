package soruhocam.com.firechannellib.model;

/**
 * Created by mertsimsek on 11/07/16.
 */
public class Message {
    public String messageId;
    public String senderId;
    public String channelId;
    public String messageText;
}
