package soruhocam.com.firechannellib.firechannel;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import soruhocam.com.firechannellib.model.Channel;
import soruhocam.com.firechannellib.model.Message;

/**
 * Created by mertsimsek on 20/06/16.
 */
public class FireChannel implements IFireChannel {

    private static FireChannel instance = null;
    FirebaseDatabase firebaseDatabase;
    String reference;

    private FireChannel(String reference) {
        firebaseDatabase = FirebaseDatabase.getInstance();
        this.reference = reference;
    }

    public static IFireChannel getInstance(String reference) {
        if (instance == null)
            instance = new FireChannel(reference);
        return instance;
    }

    @Override
    public Observable<Channel> loadChannels() {
        return Observable.create(new Observable.OnSubscribe<Channel>() {
            @Override
            public void call(final Subscriber<? super Channel> subscriber) {
                firebaseDatabase.getReference(reference)
                        .addChildEventListener(new ChildEventListener() {
                            @Override
                            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                if (dataSnapshot.getValue() != null) {
                                    Channel channel = dataSnapshot.getValue(Channel.class);
                                    subscriber.onNext(channel);
                                }
                            }

                            @Override
                            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                            }

                            @Override
                            public void onChildRemoved(DataSnapshot dataSnapshot) {

                            }

                            @Override
                            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
            }
        });
    }

    @Override
    public Observable<Channel> createChannel(final Channel channel) {
        return Observable.create(new Observable.OnSubscribe<Channel>() {
            @Override
            public void call(final Subscriber<? super Channel> subscriber) {
                String key = firebaseDatabase.getReference(reference).push().getKey();
                channel.id = key;
                firebaseDatabase.getReference(reference)
                        .child(key)
                        .setValue(channel)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(Task<Void> task) {
                                if (task.isSuccessful())
                                    subscriber.onNext(channel);
                                else
                                    subscriber.onError(new Throwable("Error while creating channel"));
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(Exception e) {
                    }
                });
            }
        });
    }

    @Override
    public Observable<Boolean> deleteChannel(final String channelId) {
        return Observable.create(new Observable.OnSubscribe<Boolean>() {
            @Override
            public void call(final Subscriber<? super Boolean> subscriber) {
                firebaseDatabase.getReference(reference)
                        .child(channelId)
                        .setValue(null)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(Task<Void> task) {
                                if (task.isSuccessful())
                                    subscriber.onNext(true);
                                subscriber.onCompleted();
                            }
                        });
            }
        });
    }

    @Override
    public Observable<Boolean> sendMessage(final String channelId, final Message message) {
        return Observable.create(new Observable.OnSubscribe<Boolean>() {
            @Override
            public void call(final Subscriber<? super Boolean> subscriber) {
                DatabaseReference databaseReference = firebaseDatabase.getReference(reference)
                        .child(channelId)
                        .child("/messages");
                String id = databaseReference.push().getKey();
                message.messageId = id;
                message.channelId = channelId;

                databaseReference.child(id)
                        .setValue(message)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful())
                                    subscriber.onNext(true);
                                else
                                    subscriber.onError(new Throwable("Error while sending message"));
                                subscriber.onCompleted();
                            }
                        });
            }
        });
    }

    @Override
    public Observable<Message> loadChannelMessages(final String channelId) {
        return Observable.create(new Observable.OnSubscribe<Message>() {
            @Override
            public void call(final Subscriber<? super Message> subscriber) {
                firebaseDatabase.getReference(reference)
                        .child(channelId)
                        .child("/messages")
                        .addChildEventListener(new ChildEventListener() {

                            @Override
                            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                Message message = dataSnapshot.getValue(Message.class);
                                subscriber.onNext(message);
                            }

                            @Override
                            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                            }

                            @Override
                            public void onChildRemoved(DataSnapshot dataSnapshot) {

                            }

                            @Override
                            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }

                        });
            }
        });
    }


}
