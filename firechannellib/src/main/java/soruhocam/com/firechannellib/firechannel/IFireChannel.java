package soruhocam.com.firechannellib.firechannel;

import java.util.HashMap;
import java.util.List;

import rx.Observable;
import soruhocam.com.firechannellib.model.Channel;
import soruhocam.com.firechannellib.model.Message;

/**
 * Created by mertsimsek on 20/06/16.
 */
public interface IFireChannel {

    Observable<Channel> loadChannels();

    Observable<Channel> createChannel(Channel channel);

    Observable<Boolean> deleteChannel(String channelId);

    Observable<Boolean> sendMessage(String channelId, Message message);

    Observable<Message> loadChannelMessages(String channelId);

}
