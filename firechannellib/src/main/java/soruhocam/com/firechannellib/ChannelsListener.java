package soruhocam.com.firechannellib;

import java.util.List;

import soruhocam.com.firechannellib.model.Channel;

/**
 * Created by mertsimsek on 20/06/16.
 */
public interface ChannelsListener {

    void onChannelListLoaded(List<Channel> channels);

    void onNewChannelCreated(Channel channel);

}
